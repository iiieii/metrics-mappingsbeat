package main

import (
	"os"

	"github.com/elastic/beats/libbeat/beat"

	"ru.sbrf.sbt/metrics/mappingsbeat/beater"
)

func main() {
	err := beat.Run("mappingsbeat", "", beater.New)
	if err != nil {
		os.Exit(1)
	}
}
