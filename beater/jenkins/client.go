package jenkins

import (
	"ru.sbrf.sbt/metrics/mappingsbeat/config"
	. "ru.sbrf.sbt/metrics/mappingsbeat/beater/functional"
	"fmt"
	"encoding/json"
	"regexp"
	"github.com/leonchen/go-async"
	"bufio"
	"net/url"
	"github.com/elastic/beats/libbeat/logp"
)

type (
	Client struct {
		BaseUrl       string     `json:"baseUrl"`
		SonarUrl      string     `json:"sonarUrl"`
		BitbucketHost string     `json:"bitbucketHost"`
		BitbucketPath string     `json:"bitbucketPath"`
		BitbucketIp   string     `json:"bitbucketIp"`
	}
	Job struct {
		Name          string     `json:"Name"`
		BitbucketUrls []string   `json:"BitbucketUrls"`
		SonarUrl      string     `json:"SonarUrl"`
	}
)

func New(jenkinsConfig *config.JenkinsConfig, sonarConfig *config.SonarConfig, bitbucketConfig *config.BitbucketConfig) *Client {

	bitbucketUrl, _ := url.Parse(bitbucketConfig.Url)
	return &Client{
		BaseUrl:       fmt.Sprintf("https://%s:%s@%s", jenkinsConfig.Username, jenkinsConfig.Token, jenkinsConfig.Host),
		SonarUrl:      sonarConfig.Url,
		BitbucketHost: bitbucketUrl.Host,
		BitbucketPath: bitbucketUrl.Path,
		BitbucketIp:   bitbucketConfig.Ip,

	}

}

func (j Client) getRequest(path string) []byte {

	return GetHttpResponseBody(
		DoGetRequest(
			fmt.Sprintf("%s%s", j.BaseUrl, path),
		),
	)

}

func (j Client) getRequestByRegex(path string, r *regexp.Regexp) string {

	res := DoGetRequest(fmt.Sprintf("%s%s", j.BaseUrl, path))

	defer func() {
		res.Body.Close()
	}()

	reader := bufio.NewReader(res.Body)
	size := 0
	for {
		line, err := reader.ReadString('\n')
		if err != nil {
			panic(err)
		}
		size += len(line)
		found := r.FindStringSubmatch(line)
		if len(found) > 1 {
			return found[1]
		}
		if size > 20000000 {
			panic(fmt.Errorf("Log file exceeded 20M"))
		}

	}
}

func (j Client) getJob(jobPath string) *apiJob {

	path := fmt.Sprintf("%s/api/json?tree=jobs[name,lastSuccessfulBuild[actions[remoteUrls,url,sonarqubeDashboardUrl]],lastCompletedBuild[actions[remoteUrls,url,sonarqubeDashboardUrl]]]", jobPath)
	body := j.getRequest(path)

	job := apiJob{}
	jsonErr := json.Unmarshal(body, &job)
	if jsonErr != nil {
		panic(fmt.Errorf("Error while parsing json (http (%s) request of '%s'): %v", "GET", fmt.Sprintf("%s%s", j.BaseUrl, path), jsonErr))
	}

	return &job
}

func (j Client) getSonarUrl(parentJobPath string, job *apiJob) (sonarUrl string) {

	defer func() {
		if r := recover(); r != nil {
			logp.Err("Error getting %s job log: %v", job.getJobName(parentJobPath), r.(error))
			sonarUrl = ""
		}
	}()

	r, _ := regexp.Compile(fmt.Sprintf("ANALYSIS SUCCESSFUL, you can browse %s/dashboard/index/(.*)", j.SonarUrl))
	r2, _ := regexp.Compile("\\r")
	logUrl := ""

	if job.LastSuccessfulBuild.Class != "" {
		logUrl = fmt.Sprintf("%s/lastSuccessfulBuild/consoleText", job.getJobName(parentJobPath))
	} else if job.LastCompletedBuild.Class != "" {
		logUrl = fmt.Sprintf("%s/lastCompletedBuild/consoleText", job.getJobName(parentJobPath))
	} else {
		return ""
	}

	res := j.getRequestByRegex(logUrl, r)
	sonarUrl = r2.ReplaceAllString(res, "")

	return sonarUrl

}

func getJenkinsJobData(data interface{}) interface{} {

	job := apiJobFromI(data.(map[string]interface{})["job"])
	parentJobPath := data.(map[string]interface{})["parentJobPath"].(string)
	j := data.(map[string]interface{})["client"].(Client)

	r, _ := regexp.Compile("^/")

	jobResult := Job{
		Name:          r.ReplaceAllString(DecodeURIComponent(job.getJobName(parentJobPath)), ""),
		BitbucketUrls: job.getBitbucketUrls(j.BitbucketHost, j.BitbucketPath, j.BitbucketIp),
		SonarUrl:      j.getSonarUrl(parentJobPath, job),
	}

	return jobResult
}

func (j Client) queryJobs(parentJobPath string) []Job {

	job := j.getJob(parentJobPath)

	jobs := async.Map(
		Map(
			Filter(apiJobArrayToI(job.SubJobs), func(job interface{}) bool {
				return apiJobFromI(job).Class != "com.cloudbees.hudson.plugins.folder.Folder"
			}),

			func(subJob interface{}) interface{} {
				return map[string]interface{}{
					"job":           subJob,
					"parentJobPath": parentJobPath,
					"client":        j,
				}
			},
		),
		getJenkinsJobData,
	)
	subJobs := Reduce(

		Filter(apiJobArrayToI(job.SubJobs), func(job interface{}) bool {
			return apiJobFromI(job).Class == "com.cloudbees.hudson.plugins.folder.Folder"
		}),

		func(prev interface{}, job interface{}) interface{} {
			subJobs := j.queryJobs(apiJobFromI(job).getJobName(parentJobPath))
			return append(prev.([]interface{}), jobArrayToI(subJobs)...)
		},

		[]interface{}{},
	)
	return append(jobArrayFromI(jobs), jobArrayFromI(subJobs.([]interface{}))...)
}

func (j Client) LoadJenkinsMapping() (jobs []Job, err error) {

	defer func() {
		if r := recover(); r != nil {
			err = r.(error)
		}
	}()

	jobs = j.queryJobs("")

	return jobs, err

}

func (j Client) GetJenkins2Bitbucket(jobs []Job) map[string]interface{} {


	return Reduce(
		jobArrayToI(jobs),

		func(prevJobs interface{}, job interface{}) interface{} {

			newJobs := CopyMap(prevJobs.(map[string]interface{}))
			jobObj := jobFromI(job)
			if len(jobObj.BitbucketUrls) > 0 {
				newJobs[jobObj.Name] = jobObj.BitbucketUrls
			}
			return newJobs
		},

		map[string]interface{}{},
	).
	(map[string]interface{})
}

func (j Client) GetSonar2Jenkins(jobs []Job) map[string]interface{} {

	return Reduce(
		jobArrayToI(jobs),

		func(prevJobs interface{}, job interface{}) interface{} {

			newJobs := CopyMap(prevJobs.(map[string]interface{}))
			jobObj := jobFromI(job)
			if jobObj.SonarUrl != "" {
				jobsList := []string{}
				if val, ok := newJobs[jobObj.SonarUrl]; ok {
					jobsList = val.([]string)
				}
				if jobsList == nil {
					jobsList = []string{}
				}
				newJobs[jobObj.SonarUrl] = append(jobsList, jobObj.Name)
			}
			return newJobs
		},

		map[string]interface{}{},
	).
	(map[string]interface{})
}

//func (j Client) toI() map[string]interface{} {
//	byteData, _ := json.Marshal(j)
//	result := map[string]interface{}{}
//	json.Unmarshal(byteData, &result)
//	return result
//}
//
//func clientFromI(i interface{}) Client {
//	byteData, _ := json.Marshal(i)
//	result := Client{}
//	json.Unmarshal(byteData, &result)
//	return result
//}

func jobFromI(job interface{}) Job {
	byteData, _ := json.Marshal(job)
	result := Job{}
	json.Unmarshal(byteData, &result)
	return result
}

func jobArrayFromI(jobs []interface{}) []Job {
	byteData, _ := json.Marshal(jobs)
	result := make([]Job, len(jobs))
	json.Unmarshal(byteData, &result)
	return result
}

func jobArrayToI(jobs []Job) []interface{} {
	byteData, _ := json.Marshal(jobs)
	result := make([]interface{}, len(jobs))
	json.Unmarshal(byteData, &result)
	return result
}
