package jenkins

import (
	"net/url"
	"regexp"
	"fmt"
	. "ru.sbrf.sbt/metrics/mappingsbeat/beater/functional"
	"encoding/json"
)

type (
	apiJob struct {
		Class               string     `json:"_class"`
		Name                string     `json:"name"`
		SubJobs             []apiJob   `json:"jobs"`
		LastSuccessfulBuild apiBuild   `json:"lastSuccessfulBuild"`
		LastCompletedBuild  apiBuild   `json:"lastCompletedBuild"`
	}
	apiBuild struct {
		Class   string      `json:"_class"`
		Actions []apiAction `json:"actions"`
	}
	apiAction struct {
		Class      string   `json:"_class"`
		RemoteUrls []string `json:"remoteUrls"`
		Url        string   `json:"url"`
	}
)

func EncodeURIComponent(str string) string {
	res, _ := url.Parse(str)
	return res.String()
}

func DecodeURIComponent(str string) string {
	res, _ := url.QueryUnescape(str)
	return res
}

func (job apiJob) getJobName(parentJob string) string {
	return fmt.Sprintf("%[1]s/job/%[2]s", parentJob, EncodeURIComponent(job.Name))
}

func (job apiJob) getBitbucketUrls(hostname string, path string, ip string) []string {

	hostAndIp := fmt.Sprintf("(%s|%s)(:[0-9]*)?(%s)?", hostname, ip, path)
	regexStr := fmt.Sprintf("(^(ssh|https?)://([^@]*@)?%s(/scm)?/|\\.git$)", hostAndIp)
	r, _ := regexp.Compile(regexStr)

	actions := []interface{}{}

	if job.LastSuccessfulBuild.Class != "" {
		actions = apiActionArrayToI(job.LastSuccessfulBuild.Actions)
	} else if job.LastCompletedBuild.Class != "" {
		actions = apiActionArrayToI(job.LastCompletedBuild.Actions)
	}

	return MapKeys(
		Reduce(
			actions,

			func(prev interface{}, action interface{}) interface{} {

				return Reduce(
					StringArrayToI(apiActionFromI(action).RemoteUrls),

					func(prev interface{}, url interface{}) interface{} {
						return Insert(prev, r.ReplaceAllString(url.(string), ""), true)
					},

					prev,
				)
			},

			map[string]interface{}{},
		).
		(map[string]interface{}))
}

//func (job apiJob) getSonarUrl() string {
//	if job.LastSuccessfulBuild.Class != "" {
//		return apiActionFromI(First(
//			apiActionArrayToI(job.LastSuccessfulBuild.Actions),
//			func(action interface{}) bool {
//				return apiActionFromI(action).Class == "hudson.plugins.sonar.BuildSonarAction"
//			},
//		)).Url
//	} else if job.LastCompletedBuild.Class != "" {
//		return ""
//	}
//	return ""
//}

func apiJobFromI(job interface{}) *apiJob {
	byteData, _ := json.Marshal(job)
	result := apiJob{}
	json.Unmarshal(byteData, &result)
	return &result
}

func apiActionFromI(action interface{}) *apiAction {
	byteData, _ := json.Marshal(action)
	result := apiAction{}
	json.Unmarshal(byteData, &result)
	return &result
}

func apiJobArrayToI(jobs []apiJob) []interface{} {
	byteData, _ := json.Marshal(jobs)
	result := make([]interface{}, len(jobs))
	json.Unmarshal(byteData, &result)
	return result
}

func apiActionArrayToI(actions []apiAction) []interface{} {
	byteData, _ := json.Marshal(actions)
	result := make([]interface{}, len(actions))
	json.Unmarshal(byteData, &result)
	return result
}