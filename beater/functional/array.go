package functional

import "reflect"

//import "reflect"

func Map(vs []interface{}, f func(interface{}) interface{}) []interface{} {
	vsm := make([]interface{}, len(vs))
	for i, v := range vs {
		vsm[i] = f(v)
	}
	return vsm
}

func Reduce(vs []interface{}, f func(interface{}, interface{}) interface{}, d interface{}) interface{} {
	res := d
	for _, v := range vs {
		res = f(res, v)
	}
	return res
}

func Filter(vs []interface{}, f func(interface{}) bool) []interface{} {
	vsf := make([]interface{}, 0)
	for _, v := range vs {
		if f(v) {
			vsf = append(vsf, v)
		}
	}
	return vsf
}

func First(vs []interface{}, f func(interface{}) bool) interface{} {
	for _, v := range vs {
		if f(v) {
			return v
		}
	}
	return nil
}

func MapKeys(m map[string]interface{}) []string {
	keys := make([]string, 0, len(m))
	for k := range m {
		keys = append(keys, k)
	}
	return keys
}

func CopyMap(m map[string]interface{}) map[string]interface{} {
	newmap := map[string]interface{}{}
	for k, v := range m {
		newmap[k] = v
	}
	return newmap
}

func Insert(m interface{}, args ...interface{}) interface{} {
	if len(args) % 2 != 0 {
		panic("must have pairs of keys and values")
	}
	mv := reflect.ValueOf(m)
	if mv.IsNil() {
		mv = reflect.MakeMap(mv.Type())
	}
	for i := 0; i < len(args); i += 2 {
		mv.SetMapIndex(reflect.ValueOf(args[i]), reflect.ValueOf(args[i+1]))
	}
	return mv.Interface()
}

func StringArrayToI(arr []string) []interface{} {
	res := make([]interface{}, len(arr))
	for i, v := range arr {
		res[i] = v
	}
	return res
}