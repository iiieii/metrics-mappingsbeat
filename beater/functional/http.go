package functional

import (
	"net/http"
	"crypto/tls"
	"io/ioutil"
	"fmt"
	"time"
)

func getHttpTransport() *http.Transport {
	return &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
	}
}

func getHttpClient() *http.Client {
	return &http.Client{
		Transport: getHttpTransport(),
		Timeout: time.Duration(15 * time.Second),
	}
}

func DoHttpRequest(method, urlStr string) *http.Response {

	req, reqErr := http.NewRequest(method, urlStr, nil)
	if reqErr != nil {
		panic(fmt.Errorf("Error while creating http (%s) request of '%s': %v", method, urlStr, reqErr))
	}

	res, resErr := getHttpClient().Do(req)
	if resErr != nil {
		panic(fmt.Errorf("Error while doing http (%s) request of '%s': %v", method, urlStr, resErr))
	}

	return res
}

func DoGetRequest(urlStr string) *http.Response {
	return DoHttpRequest("GET", urlStr)
}

func GetHttpResponseBody(res *http.Response) []byte {

	body, respErr := ioutil.ReadAll(res.Body)

	if respErr != nil {
		panic(fmt.Errorf("Error while parsing response body (http (%s) request of '%s'): %v", res.Request.Method, res.Request.URL, respErr))
	}

	if res.StatusCode != 200 {
		panic(fmt.Errorf("Response (http (%s) request of '%s') status: %s; Response body: %s", res.Request.Method, res.Request.URL, res.Status, string(body)))
	}

	return body
}