package beater

import (
	"fmt"
	"time"

	"github.com/elastic/beats/libbeat/beat"
	"github.com/elastic/beats/libbeat/common"
	"github.com/elastic/beats/libbeat/logp"
	"github.com/elastic/beats/libbeat/publisher"

	"ru.sbrf.sbt/metrics/mappingsbeat/config"
	"ru.sbrf.sbt/metrics/mappingsbeat/beater/jenkins"
)

type Mappingsbeat struct {
	done          chan struct{}
	config        config.Config
	client        publisher.Client
	jenkinsClient *jenkins.Client
}

// Creates beater
func New(b *beat.Beat, cfg *common.Config) (beat.Beater, error) {
	globalCfg := config.DefaultConfig
	if err := cfg.Unpack(&globalCfg); err != nil {
		return nil, fmt.Errorf("Error reading config file: %v", err)
	}

	if globalCfg.Jenkins == nil {
		return nil, fmt.Errorf("Could not find jenkins in config")
	}
	if globalCfg.Sonar == nil {
		return nil, fmt.Errorf("Could not find sonar in config")
	}
	if globalCfg.Bitbucket == nil {
		return nil, fmt.Errorf("Could not find bitbucket in config")
	}

	bt := &Mappingsbeat{
		done:          make(chan struct{}),
		config:        globalCfg,
		jenkinsClient: jenkins.New(globalCfg.Jenkins, globalCfg.Sonar, globalCfg.Bitbucket),
	}
	return bt, nil
}

func (bt *Mappingsbeat) Run(b *beat.Beat) error {
	logp.Info("mappingsbeat is running! Hit CTRL-C to stop it.")

	bt.client = b.Publisher.Connect()
	ticker := time.NewTicker(bt.config.Period)
	for {
		select {
		case <-bt.done:
			return nil
		case <-ticker.C:
		}

		jobs, err := bt.jenkinsClient.LoadJenkinsMapping()
		if err != nil {
			logp.Err("Error trying to get mappings from jenkins: %v", err)
		} else {
			sonar2jenkins := bt.jenkinsClient.GetSonar2Jenkins(jobs)
			for k, v := range sonar2jenkins {
				event := common.MapStr{
					"type":            "sonar2jenkins",
					"sonarProjectKey": k,
					"jenkinsJobPaths": v,
				}
				bt.client.PublishEvent(event)
				logp.Info("Event sent")
			}
			jenkins2bitbucket := bt.jenkinsClient.GetJenkins2Bitbucket(jobs)
			for k, v := range jenkins2bitbucket {
				event := common.MapStr{
					"type":            "jenkins2bitbucket",
					"jenkinsJobPath": k,
					"bitbucketRepos": v,
				}
				bt.client.PublishEvent(event)
				logp.Info("Event sent")
			}
		}
	}
}

func (bt *Mappingsbeat) Stop() {
	bt.client.Close()
	close(bt.done)
}
