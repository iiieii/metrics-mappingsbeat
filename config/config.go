// Config is put into a different package to prevent cyclic imports in case
// it is needed in several locations

package config

import (
	"time"
)

type Config struct {
	Period    time.Duration  `config:"period"`
	Jenkins	  *JenkinsConfig `config:"jenkins"`
	Sonar     *SonarConfig `config:"sonar"`
	Bitbucket *BitbucketConfig `config:"bitbucket"`
}

var DefaultConfig = Config{
	Period: 1 * time.Second,
}

type JenkinsConfig struct {
	Host   	  string        `config:"host"`
	Username  string        `config:"username"`
	Token     string        `config:"token"`
}

var JenkinsDefaultConfig = JenkinsConfig{
	Host: "",
	Username: "",
	Token: "",
}

type SonarConfig struct {
	Url   	  string        `config:"url"`
}

var SonarDefaultConfig = SonarConfig{
	Url: "",
}

type BitbucketConfig struct {
	Url   	  string        `config:"url"`
	Ip        string        `config:"ip"`
}

var BitbucketDefaultConfig = BitbucketConfig{
	Url: "",
	Ip:  "",
}